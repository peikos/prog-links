---
title: Video's
subtitle: Links naar opgenomen colleges op YouTube
comments: false
date: 2020-10-01
---

### Les 1/2
{{< youtube pJXqUpJlcN0 >}}

### Les 3
{{< youtube owugGAOFH5g >}}

### Les 4
{{< youtube l9JkF7T2oOw >}}

### Les 5
{{< youtube s4Udxp1SGJ0 >}}

### Les 6
{{< youtube t_wybrOfP68 >}}

### Les 7
{{< youtube hi2qjG_xZpQ >}}

### Les 8
{{< youtube umT1hkT6UcE >}}

### Les 9
{{< youtube XbtsuYJsMhU >}}

### Les 10
{{< youtube -fEgBVPXuEk >}}