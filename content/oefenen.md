---
title: Oefenen
subtitle: Sites met programmeer-oefeningen
comments: false
date: 2020-10-01
---

- [CodeWars](https://www.codewars.com/?language=python)
- [CodinGame](https://www.codingame.com/start)
- [Advent of Code](https://adventofcode.com/) (gevorderd, kerst)
