---
title: Oefenen
subtitle: Sites met programmeer-oefeningen
comments: false
draft: false
date: 2020-10-01
---

## Programmeren
- [CodeWars](https://www.codewars.com/?language=python)
- [CodinGame](https://www.codingame.com/start)
- [Advent of Code](https://adventofcode.com/) (gevorderd, kerst)

## Wiskunde
Voor wie AI overweegt is op zich geen wiskundige voorkennis nodig, maar zal wiskunde wel in veel vakken centraal staan, vaak in combinatie met programmeren. Als je je vast voor wil bereiden hierop, en/of wil zien of dit is voor jou is dan heb ik in de les Khan Academy aangeraden. Hier kun je gratis cursussen volgen over verschillende wiskundige onderwerpen, met video lectures en automatisch beoordeelde toetsen. Hieronder een lijst met onderwerpen die relevant zijn ter voorbereiding:

- [Trigonometry (trig functions)](https://www.khanacademy.org/math/trigonometry/unit-circle-trig-func)
- [Algebra](https://www.khanacademy.org/math/algebra)
- [Precalculus](https://www.khanacademy.org/math/precalculus)

Zeker de laatste twee zijn flinke onderwerpen, waarvan we lang niet alle onderdelen zullen behandelen en delen (vectoren en matrices) in latere vakken aan bod zullen komen. Kijk vooral wat je leuk lijkt en welke onderdelen je eventueel al kent, we gaan er zeker niet vanuit dat iedereen dit allemaal doorgewerkt heeft. Veel plezier!